# HSK 2012-2021 Standard Course Pleco Flashcards

[Pleco](https://www.pleco.com) is a Chinese language reference and learning app that has a paid flashcard system add-on. It has been the most convenient flashcard system I have used so far to learn Chinese. I use it to learn new vocabulary and to prepare for the [Chinese Proficiency Tests](https://en.wikipedia.org/wiki/Hanyu_Shuiping_Kaoshi). This repository contains flashcards for the Pleco flashcard systems based on the vocabulary in [HSK Standard Course books](https://www.blcup.com/EnSeriesBook/index/8). In time I will add other HSK test levels.

加油!

P.S. Enjoy the journey and don't focus too much on the destination. :)

## Usage
Each HSK test level is a category and contains several subcategories; one subcategory for each lesson in the HSK standard course books. The vocabulary of a lesson can be turned on/off to customize your tests based on your personal study plan (e.g. one lesson a week).

How to use these flashcards? Pleco has a quite extensive [instruction manual for the flashcard system](https://iphone.pleco.com/manual/30200/flash.html).

## Export Flashcards
The flashcards have been exported using the following settings:

* CARDS
  * Export:			cards in categories
  * Categories:			select all lessons within one test level
* FILE FORMAT
  * File format:			Text File
  * Text encoding:		UTF-8
  * Character set:		Simplified
* INCLUDE DATA
  * Categories:			ON
  * Cantonese:			OFF
  * Card definitions:		OFF
  * Dictionary definitions:	OFF

## Import Flashcards
I have successfully imported the flashcards on multiple devices using the following settings:

* FILE FORMAT
  * Text encoding:		UTF-8
* DEFINITIONS
  * Definition source:		Prefer Dicts
  * Dictionaries:		All
  * Store in user dict:		OFF
* PROBLEM HANDLING
  * Fill in missing fields:	ON
  * Missing entries:		Create Blank
  * Ambiguous entries:		Use First
  * Duplicate entries:		Merge Cats

## License
GNU GPLv3
